# coding=utf-8

import django.dispatch


field_to_resize = django.dispatch.Signal(providing_args=["field", "sizes"])
resize_now = django.dispatch.Signal(providing_args=["field", "size"])
