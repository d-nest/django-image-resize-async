# coding=utf-8
from django.db import models

from imageresizeasync.settings import ImageResizeAsyncConf
from imageresizeasync.base.signals import field_to_resize


def resize_image_save_decorator(func, fields, sizes):
    def save(self, *args, **kwargs):
        fields_to_commit = []
        for field in fields:
            field_instance = eval('self.%s' % field)
            if field_instance and not field_instance._committed:
                fields_to_commit.append(field)
        ret = func(self, *args, **kwargs)
        for field in fields_to_commit:
            field_to_resize.send(sender=self, field=field, sizes=sizes[field])
        return ret
    return save


class ResizeImageModelBase(models.base.ModelBase):
    def __new__(cls, name, bases, attrs):
        super_new = super(ResizeImageModelBase, cls).__new__

        module = attrs['__module__']
        new_class = super_new(cls, name, bases, attrs)

        try:
            fields = new_class.IMAGE_FIELDS
        except:
            raise RuntimeError(
                "Model class %s.%s doesn't have the IMAGE_FIELDS field" %
                (module, name)
            )

        try:
            sizes = new_class.IMAGE_SIZES
        except:
            raise RuntimeError(
                "Model class %s.%s doesn't have the IMAGE_SIZES field" %
                (module, name)
            )

        try:
            default = new_class.IMAGE_DEFAULT
        except:
            default = {}

        conf = ImageResizeAsyncConf()
        for field in fields:
            try:
                field_sizes = sizes[field]
            except:
                raise RuntimeError(
                    "Model class %s.%s doesn't have defined %s in the IMAGE_SIZES field" %
                    (module, name, field)
                )

            for size in field_sizes:
                new_class.add_to_class(
                    '%s_%s_%s' % (field, size[0], size[1]),
                    models.ImageField(upload_to=conf.IMAGES_PATH, blank=True, null=True,
                                      default=default.get(field, None))
                )

        new_class.save = resize_image_save_decorator(new_class.save, fields, sizes)

        return new_class
