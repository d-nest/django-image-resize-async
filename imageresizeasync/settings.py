# -*- coding: utf-8 -*-
from appconf import AppConf


class ImageResizeAsyncConf(AppConf):
     IMAGES_PATH = 'thumbnails'

     RABBITMQ_QUEUE_HOST = 'localhost'
     RABBITMQ_QUEUE_NAME = 'resize_image_async'
     RABBITMQ_QUEUE_DURABLE = True
     RABBITMQ_QUEUE_DELIVERY_MODE = 2
     RABBITMQ_QUEUE_PREFETCH_COUNT = 1
     RABBITMQ_QUEUE_NO_ACK = False
     RABBITMQ_TRHEAD_CONFIG_NAME = 'resize_image_async_consumer'
     RABBITMQ_MAX_THREADS = 1

     SORL_CROP_TYPE = 'center'
     SORL_QUALITY = 99
     SORL_FORMAT = 'PNG'

     class Meta:
         proxy = True

