# coding=utf-8
from django.dispatch import receiver

from imageresizeasync.base.signals import field_to_resize, resize_now


@receiver(field_to_resize)
def send_message(sender, **kwargs):
    field = kwargs['field']
    sizes = kwargs['sizes']

    for size in sizes:
        resize_now.send(sender=sender, field=field, size=size)
    
