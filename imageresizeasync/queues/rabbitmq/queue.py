# coding=utf-8
from django.dispatch import receiver

import pika

from imageresizeasync.settings import ImageResizeAsyncConf
from imageresizeasync.base.signals import field_to_resize


@receiver(field_to_resize)
def send_message(sender, **kwargs):
    from django.contrib.contenttypes.models import ContentType
    
    field = kwargs['field']
    sizes = kwargs['sizes']

    conf = ImageResizeAsyncConf()

    connection = pika.BlockingConnection(pika.ConnectionParameters(host=conf.RABBITMQ_QUEUE_HOST))
    channel = connection.channel()
    channel.queue_declare(queue=conf.RABBITMQ_QUEUE_NAME, durable=conf.RABBITMQ_QUEUE_DURABLE)

    ctype = ContentType.objects.get_for_model(sender)

    for size in sizes:
        channel.basic_publish(exchange='',
                              routing_key=conf.RABBITMQ_QUEUE_NAME,
                              body=str({
                                  'model_string': '%s.%s' % (ctype.app_label, ctype.model),
                                  'id': sender.id,
                                  'field': field,
                                  'size': size
                              }),
                              properties=pika.BasicProperties(delivery_mode=conf.RABBITMQ_QUEUE_DELIVERY_MODE))
