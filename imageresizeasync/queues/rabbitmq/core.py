# coding=utf-8
import os
import signal
import subprocess

from imageresizeasync.settings import ImageResizeAsyncConf


def ps_output():
    ps = subprocess.Popen(['ps', 'aux'], stdout=subprocess.PIPE)
    out, err = ps.communicate()

    try:
        return out.decode('utf-8')
    except:
        return out


def get_status():
    """
    Obtains the workers process status based on its configuration file.
    """
    workers = []
    out = ps_output()

    conf = ImageResizeAsyncConf()

    for line in out.splitlines():
        if conf.RABBITMQ_TRHEAD_CONFIG_NAME  in line:
            workers.append(line)
    return workers


def stop_workers():
    """
    Stops all workers process based on its config.
    """
    workers_count = 0
    out = ps_output()

    conf = ImageResizeAsyncConf()

    for line in out.splitlines():
        if conf.RABBITMQ_TRHEAD_CONFIG_NAME in line:
            pid = int(line.split(None)[1])
            os.kill(pid, signal.SIGKILL)
            workers_count += 1

    return workers_count
