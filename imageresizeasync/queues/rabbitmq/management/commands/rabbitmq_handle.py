# coding=utf-8
from django.core.management.base import BaseCommand

from imageresizeasync.settings import ImageResizeAsyncConf
from imageresizeasync.queues.rabbitmq.core import *


class Command(BaseCommand):
    help = 'Handle of rabbitmq'

    def handle(self, *args, **options):
        method = args[0]

        conf = ImageResizeAsyncConf()

        self.stdout.write('settings.TRHEAD_CONFIG_NAME = %s' % conf.RABBITMQ_TRHEAD_CONFIG_NAME)

        if method == 'stop':
            self.stdout.write("Stop '%d' rmq workers" % stop_workers())
        elif method == 'status':
            workers = get_status()
            for line in workers:
                self.stdout.write(line)
            self.stdout.write("Running '%d' rmq workers ..." % len(workers))

        self.stdout.write('-- End --')
