# coding=utf-8
try:
    from django.core.management.base import NoArgsCommand
except ImportError:
    from django.core.management import BaseCommand as NoArgsCommand
from django.utils.six import string_types
from django.apps import apps
from django.db.models import Model

import pika

from imageresizeasync.settings import ImageResizeAsyncConf
from imageresizeasync.base.signals import resize_now


def resolve_model_string(model_string, default_app=None):
    """
    Resolve an 'app_label.model_name' string into an actual model class.
    If a model class is passed in, just return that.
    Raises a LookupError if a model can not be found, or ValueError if passed
    something that is neither a model or a string.
    :param model_string
    :param default_app
    """
    if isinstance(model_string, string_types):
        try:
            app_label, model_name = model_string.split(".")
        except ValueError:
            if default_app is not None:
                # If we can't split, assume a model in current app
                app_label = default_app
                model_name = model_string
            else:
                raise ValueError("Can not resolve {0!r} into a model. Model names "
                                 "should be in the form app_label.model_name".format(
                                     model_string), model_string)

        return apps.get_model(app_label, model_name)

    elif isinstance(model_string, type) and issubclass(model_string, Model):
        return model_string
    else:
        raise ValueError("Can not resolve {0!r} into a model".format(model_string), model_string)


def callback(ch, method, properties, body):
    data = eval(body)

    model_string = data['model_string']
    model_class = resolve_model_string(model_string)
    sender = model_class.objects.get(id=data['id'])

    resize_now.send(sender=sender, field=data['field'], size=data['size'])

    ch.basic_ack(delivery_tag=method.delivery_tag)


class Command(NoArgsCommand):
    help = 'Consume message from rabbitmq queue...'

    def handle(self, *args, **options):
        conf = ImageResizeAsyncConf()
        connection = pika.BlockingConnection(pika.ConnectionParameters(host=conf.RABBITMQ_QUEUE_HOST))

        channel = connection.channel()
        channel.queue_declare(queue=conf.RABBITMQ_QUEUE_NAME, durable=conf.RABBITMQ_QUEUE_DURABLE)

        channel.basic_qos(prefetch_count=conf.RABBITMQ_QUEUE_PREFETCH_COUNT)
        channel.basic_consume(callback, queue=conf.RABBITMQ_QUEUE_NAME, no_ack=conf.RABBITMQ_QUEUE_NO_ACK)
        channel.start_consuming()
