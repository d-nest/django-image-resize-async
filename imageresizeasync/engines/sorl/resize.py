# coding=utf-8
from django.dispatch import receiver
from django.core.files.base import ContentFile

from sorl.thumbnail import get_thumbnail

from imageresizeasync.settings import ImageResizeAsyncConf
from imageresizeasync.base.signals import resize_now


@receiver(resize_now)
def resize(sender, **kwargs):
    field = kwargs['field']
    size = kwargs['size']

    conf = ImageResizeAsyncConf()

    field_instance = eval('sender.%s' % field)
    field_to_save = eval('sender.%s_%s_%s' % (field, size[0], size[1]))

    if field_instance:
        im = get_thumbnail(
            field_instance.file,
            '%sx%s' % (size[0], size[1]),
            crop=conf.SORL_CROP_TYPE,
            quality=conf.SORL_QUALITY,
            format=conf.SORL_FORMAT
        )

        field_to_save.save(
            im.name,
            ContentFile(im.read())
        )

