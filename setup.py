# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

VERSION = __import__('imageresizeasync').get_version()


setup(
    name='imageresizeasync',
    version=VERSION,
    url='https://bitbucket.org/abalt/django-image-resize-async',
    author='DNest',
    author_email='admin@dnesteagency.com',
    description=(
        "Django Image Resize Async Library"),
    long_description=open('README.rst').read(),
    keywords="django image resize async",
    license=open('LICENSE').read(),
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'django>=1.8.9',
        'django-appconf>=1.0.2'
    ],
    download_url='https://bitbucket.org/abalt/screator/get/0.1.8.zip',
    # See http://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Topic :: Other/Nonlisted Topic'],
)
